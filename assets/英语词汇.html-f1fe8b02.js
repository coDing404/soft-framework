import{_ as e,X as n,Y as i,a2 as s}from"./framework-5b727e0e.js";const d={},l=s(`<h2 id="超高频词汇" tabindex="-1"><a class="header-anchor" href="#超高频词汇" aria-hidden="true">#</a> 超高频词汇</h2><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code> access 访问
 analysis/analyze/analyst n分析分析/n.分析师
 application 应用，申请，申请书
 architecture架构，体系结构
 assign 分配
 cient 客户
 Collection 收集
 component 组件，构件，成分
 computer/compute n.计算机，电脑/v.计算
 connect/connection v连接/n.联系
 connection/connect/connector n.联系/联系/n连接器
 constraint 约束
 construct/construction/reconstruction v.修建，创建n.建造/n.重建
 contain 包含
 CPU(Central Processing Unit) 中央处理器
 database 数据库
 data structure 数据结构
 define 定义界定
 design 设计
 detailed/detail adi详细设计的/n.细节
 develop/development v.发展，开发/n发展
 device装置
 DFD (Data flow diagram) 数据流图
 distributed/distribute adj分布式的/.分发，分配
 document 文件，文献，文档
 domain 领域，范围
 element 元素，要素
 embody 体现
 enterprise 企
 file 文件
 format/form 格式/类型
 functional/function/nonfunctional adi.功能性的/n功能/adi非功能性的
 group 群，组
 hardware 硬件
 implement/implementation v.实施/n.实施
 忐孱免识界赦睁拘岌坷奚浩浣施碧rmation抒 信息
 interactive/interaction adj交与的/n.交互作用
 interface接口
 involve涉及
 /O(input/output) 输入/输出
 layer 层，层次
 logic逻辑
 manage/management v.管理/n.经营
 memory内存
 model/modeling n.模型/v.建模
 normalization 规范化
 normalform范式
 object/objective n.对象，物品/adi.客观的，n.目标 operate/Operational v操作，运转/adj.可操作的
 optimization/optimized n.优化优化
 overall 总体的，全部的
 Partitioning 分割，划分
 pattern 模式，典范
 phase 阶段，时期
 physical 物理的
 primary(primary key)主要的，重要的(主键)
 processor/process n处理器/处理，n进程
 property 性质，财产
 record记录
 relate 联系，涉及
 represent/representation v.代表/n.表现
 replication 复制
 requirement/require n.需求/v.要求
 schema提要，模式
 server 服务器
 software 软件
 specification/specify/specific n.规格，规范/.明确规定/adj.具体的
 speed 速度
 standard标准
 storage /store n存储/v.存储
 system系统
 table 表格
 technology 技术，科技
 tier 层，级
 transaction 事务
 view 视图
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="高频词汇" tabindex="-1"><a class="header-anchor" href="#高频词汇" aria-hidden="true">#</a> 高频词汇</h2><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code> actor 参与者
 adjunct term 附加术语
 allocate 分配
 attribute 属性
 audit 审计
 augment 加强
 artifact 人工制品
 backup 备份
 basic基本的，基础
 browser浏览器
 cable 电缆，缆缉
 capacity 容量
 category 种类，类别
 Cloud platform 云平台
 Cluster system 集群系统
 code代码
 command命令
 concern 涉及
 concurrency 并发，并发处理
 conjunction 结合
 connector连接器
 constitute 构成，组成
 consultant 顾问
 convert 转换，转变
 data dictionary 数据字典
 data layers 数据层
 data manipulation 数据操作
 data redundancy 数据几余
 DBMS关系数据库管理系统
 decomposition 分解
 deliverable 可交付的可交付物
 deployment部署
 dialogue 对话
 draw 绘制
 dynamic动态的
 electronic 电子的
 entity 实体
 entity-relationship diagram(ERD)实体关系图
 establish 建立
 evaluate/reevaluate 评估/重新评估
 execute 执行，实施
 expand/expansion 扩展
 extraction 提取
 facility 设备，设施
 Fat cient 胖客户端
 feasibility 可行性
 field 领域
 foreign key 外键
 fundamental基本的，基础的
 fusion 融合
 framework 框架，体系结构，架构
 general音遍的，概要的
 guideline 指导方针，准则
 hard disk 硬盘
 hierarchical分层的，分等级的
 horizontalreplication 水平复制
 identify 识别，确认
 infrastructure 基础设施
 initially 开始
 instruction 指示
 integrity 完整性
 intentional 故意的
 internal内部的
 internet 因特网，互联网
 interpretive 解释的
 intranet内联网
 iterative 迭代的
 layout布局
 link 链接，联系
 list 列表
 maintainability 可维护性
 manipulate 操纵
 map/mapping n地图/v.映射
 mechanism 机制
 memo 备忘录
 message消息
 metadata 元数据
 method 方法
 modular/module adi模块化的/n模块
 multidimensional 多维的，多方位的
 MVC(Model-View-Controller) 模型视图控制器
 notation form 符号格式
 notebook 笔记本电脑
 organize组织
 oriented 面向
 outdate过时
 persistence 持久
 Pipes and Filters 管道-过滤器
 position 位置
 potential 潜在的
 principle 原则，准则
 procedure 程序
 product-line 产品线
 progression 进展，进程
 proposal提议，提案
 prototype原型
 quality 质量
 query 查询
 RDBMS(Relational Database Manaqement System)关系型数据库管理系统
 reconcile 调和
 referential 参照的，参考的
 reflection 反射
 refine 精炼
 reliability 可靠性
 response 响应，回复
 retrieve 取回，检索
 reverse engineering 逆向工程
 safety 安全，平安
 schedule进度，日程安排
 scope范围
 security 安全，担保
 segment 段，片，分割
 semantic语义的
 static 静态的
 subset 子集，分组
 subsystem 子系统
 synchronize 同步
 Thin client 瘦客户端
 topology 拓扑结构
 unified/unify adj统一的/.统
 use case 用例
 valid 有效的
 validation 验证
 value 值
 vendor供应商，小贩
 vertical partitioning 垂直分区
 virus 病毒
 vocabulary 词汇
 web site 网址，网站
 workstation 站
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h1 id="名词解释" tabindex="-1"><a class="header-anchor" href="#名词解释" aria-hidden="true">#</a> 名词解释</h1><h2 id="企业信息化战略与实施" tabindex="-1"><a class="header-anchor" href="#企业信息化战略与实施" aria-hidden="true">#</a> 企业信息化战略与实施</h2><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>企业系统规划法(BSP): Business System Planning.
关键成功因素法(CSF): Critical Success Factors.
战略集合转化法(SST): Strategy Set Transformation.
管理信息系统(MIS): Management Information System.
战略数据规划法(SDP): Strategic Data Planning.
信息工程法(E): Information Engineering.
战略栅格法(SG) : Strategic Grid.
价值链分析法(VCA): Value Chain Analysis.
战略一致性模型(SAM) : Strategic Alignment Model.
政府对政府(GG): Government To Government.
政府对企业(GB)或BG: Government To Business
政府对公民(GC)或CG: Government To Citizen.
政府对公务员(GE):Government To Employee.
物料需求计划(MRP): Material Requirement Planning.
制造资源计划(MRPII) Manufacturing Resource Planning Il.
企资源计划(ERP): Enterprise Resource Planning.
客户关系管理(CRM): Customer Relationship Management.
供应链管理(SCM): Supply Chain Management.
商业智能(BI): Business Intelligence
联机分析处理 (OLAP): On-Line Analytic Processinge
决策支持系统(DSS):Decision-making Support System
业务流程重组(BPR): Business Process Reengineering.
业务流程管理(BPM): Business Process Management
PCDA闭环(Plan(计划)、Do(执行)、Check(检查)、Act(处理)).
APl: Application Programming Interface.
企业信息门户(EIP): Enterprise Information Portal.
企业知识门户(EKP): Enterprise Knowledge Portal.
企业应用门户(EAP): Enterprise Application Portal.
企业对消费者(BC): Business To Customer.
企业对企业(BB): Business To Business.
消费者对消费者(CC): Customer To Customer.
线上对线下(O): Online To Ofline.
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="软件工程" tabindex="-1"><a class="header-anchor" href="#软件工程" aria-hidden="true">#</a> 软件工程</h2><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>基于构件的开发: Component-Based Software Development CBSD，有时也称为基于构件的软件工程CBSE
极限编程(XP): Extreme Programming.
Coad的功用驱动开发方法: FDD-Feature Driven Development.
ASD方法(ASD): Adaptive Software Development.
结构化分析(SA): Structured Analysis.
数据流图(DFD): Data Flow Diagram.
状态转移图(STD) : State Transition Diagram.
实体关系图(ERD/E-R图/ER图): Entity Relationship Diagram.
面向对象分析(OOA): Object-Oriented Analysis.
统一建模语言(UML) : Unified Modeling Language.
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="软件架构设计" tabindex="-1"><a class="header-anchor" href="#软件架构设计" aria-hidden="true">#</a> 软件架构设计</h2><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>客户端-服务器结构(C/S，Client/Server)
浏览器-服务器结构(B/S，Browser/Server)
MVC (Model View Controller，模型(model) - 视图(view) - 控制器(controller))
MVP (Model-View-Presenter，Model提供数据，View负责显示，Controller/Presenter负责逻辑的处理)MVVM: Model-View-ViewModel
对象关系映射(ORM):bject Relational Mapping.
富互联网应用(RIA): Rich Internet Application.
企业服务总线 (ESB): Enterprise Service Bus.
简单对象访问协议(SOAP): Simple ObjectAccess Protocol
模型驱动架构(MDA): Model Driven Architecture.
平台独立模型(PIM): Platform Independent Models.
平台相关模型(PSM): Platform Specific Models.
架构描述语言 (ADL):Architecture Description Language
特定领域软件架构(DSSA): Domain Specific Software Architecture.
基于架构的软件设计(ABSD): Architecture-Based Software Design.
软件架构分析法(SAAM): Software Architecture Analysis Method.
架构权衡分析法(ATAM): Architecture Tradeoff Analysis Method.
成本效益分析法(CBAM): the Cost Benefit Analysis Method.
对象请求代理 (ORB): Object Request Broker.
扩展标记语言(XML) : Extensible Markup Language.
JSON: JavaScript Object Notation.
表述性状态转移(REST): Representational State Transfer.
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="系统安全分析与实践" tabindex="-1"><a class="header-anchor" href="#系统安全分析与实践" aria-hidden="true">#</a> 系统安全分析与实践</h2><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>公开密钥基础设施(PKI)：Public Key Infrastructure
认证中心(CA): Certificate Authority
注册中心(RA): Registration Authority
优良保密协议(PGP): Pretty Good Privacy
安全套接字协议(SSL): Secure Sockets Layer
传输层安全协议(TLS): Transport Layer Security
安全电子交易协议(SET): Secure Electronic Transaction
互联网安全协议(IPSEC): Internet Protocol Security 
可扩展的访问控制标记语言 (XACML): eXtensible Access Control Markup Language.
自主访问控制(DAC): Discretionary Access Control.
访问控制列表(ACL): Access Control List.
强制访问控制(MAC): Mandatory Access Control.
甚于角色访问控制(RBAC): Role-Based Access Control.
基于任务的访问控制(TBAC): Task-BasedAccess Control
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="系统可靠性分析与设计" tabindex="-1"><a class="header-anchor" href="#系统可靠性分析与设计" aria-hidden="true">#</a> 系统可靠性分析与设计</h2><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>平均无故障时间(MTTF): Mean Time To Failure.
平均故障修复时间(MTTR): Mean Time To Repair.
平均故障间隔时间(MTBF): Mean Time Between Failure.
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="项目管理" tabindex="-1"><a class="header-anchor" href="#项目管理" aria-hidden="true">#</a> 项目管理</h2><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>工作分解结构(WBS):Work Breakdown Structure.
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><h2 id="计算机组成与体系结构" tabindex="-1"><a class="header-anchor" href="#计算机组成与体系结构" aria-hidden="true">#</a> 计算机组成与体系结构</h2><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>单指令流单数据流(SISD):Single instruction, Single data.
单指令流多数据流(SIMD)::Single instruction, multiple data
多指令流单数据流(MISD): multiple instruction, Single data.
多指令流多数据流(MIMD) : multiple instruction, multiple data.
复杂指令集计算机(CISC): Complex Instruction Set Computers
精简指令集计算机(RISC): Reduced Instruction Set Computers.
数字信号处理(DSP): Digital Signal Processing.
中央处理器(CPU): Central Processing Unit.
随机存取存储器(RAM): Random Access Memory.
只读存储器(ROM): Read-Only Memory.
动态RAM(DRAM)：Dynamic RAM
静态RAM(SRAM)：Static RAM
掩模式ROM(MROM) ：Mask ROM
一次可编程(PROM)：Programmable ROM
可擦除的PROM(EPROM)：EPROM Erasable PROM
闪速存储器 (flash memory，闪存)
先来先服务(FCFS): First-Come. First Served.
最短寻道时间优先(SSTF):Shortest Seek Time First.
吞叶率(TP) : Through Put rate.
数据总线(DB): Data Bus.
地址总线(AB): Address Bus.
控制总线(CB): Control Bus.
片上系统(SoC): System on Chip.
微控制单元(MCU): Microcontroller Unit
微处理器(MPU): Microcontroller CPU.
循环几余校验(CRC): Cyclic Redundancy Check.
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="系统配置与性能评价" tabindex="-1"><a class="header-anchor" href="#系统配置与性能评价" aria-hidden="true">#</a> 系统配置与性能评价</h2><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>平均每条指令的平均时钟周期个数(CPI): dock per instruction
每(时钟)周期运行指令条数(IPC) : instruction per clock.
百万条指令每秒(MIPS) : Million Instructions Per Second.
每秒百万个浮点操作(MFLOPS): Million Floating-point Operations per Second.
响应时间(RT): Response Time.
数据处理速率法(PDR): Processing Data Rate.
综合理论性能法(CTP): Composite Theoretical Performance.
事务处理委员会(TPC): Transaction Processing Council.
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="操作系统" tabindex="-1"><a class="header-anchor" href="#操作系统" aria-hidden="true">#</a> 操作系统</h2><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>直接存储器访问(DMA): Direct Memory Access.
输入/输出(I/O): Input/Output.
实时操作系统(RTOS):Real-Time Operating System.
操作系统(OS): Operating System.
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="计算机网络" tabindex="-1"><a class="header-anchor" href="#计算机网络" aria-hidden="true">#</a> 计算机网络</h2><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>网际互连协议 (IP): Internet Protocol.
专输控制协议(TCP): Transmission Control Protocol.
用户数据报协议(UDP): User Datagram Protocol.
动态主机配置协议(DHCP): Dynamic Host Configuration Protocol.
域名系统(DNS): Domain Name System.
公用交换电话网络(PSTN) : Public Switched Telephone Network.
数字数据网(DDN): Data Direct Networks.
综合业务数字网(ISDN): Integrated Services Digital Network.
非对称数字用户线路(ADSL): Integrated Services Digital Network.
同轴光纤技术(HFC): Hybrid Fiber Coaxial.
无线局域网鉴别和保密基础结构(WAPI): Wireless LAN Authentication and Privacy nfrastructure.
码分多址(CDMA) : Code Division Multiple Access.
宽带码分多址(WCDMA) : Wideband Code Division Multiple Access.
时分同步码分多址(TD-SCDMA): Time Division-Synchronous Code Division MultipleAccess.
长期演进技术(LET): Long Term Evolution.
时分双工(TDD): Time-division duplex.
频分双工(FDD): Frequency-division duplex.
直连式存储(DAS): Direct-Attached Storage.
网络附加存储(NAS): Network-Attached Storage
存储区域网络(SAN): Storage Area Network.
Internet小型计算机系统接口 (iSCSI) : Interet Small Computer System Interface.
磁盘阵列(RAID): Redundant Arrays ofIndependent Disk.
射频识别技术(RFID): Radio Frequency ldentification.
软件即服务(SaaS): Software-as-a-Service.
平台即服务(PaaS): Platform-As-A-Service.
基础设施即服务(laaS): Infrastructure-As-A-Service.
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="数据库系统" tabindex="-1"><a class="header-anchor" href="#数据库系统" aria-hidden="true">#</a> 数据库系统</h2><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>数据库管理员(DBA): Database Administrator.
数据库管理系统(DBMS): Database Management System.
局部数据库管理系统(LDBMS): Local Database Management System.
全局数据库管理系统(GDBMS): Gobal Database Management System.
通信管理(CM): Communication Management.
分布式数据库管理系统(DDBMS): Distributed Database Management System.
联邦数据库系统(FDBS): Federated Database System.
联邦数据库管理系统(FDBMS): Federated Database Management System.
非关系数据库(NoSQL): Not-only SQL
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,27),a=[l];function r(v,c){return n(),i("div",null,a)}const u=e(d,[["render",r],["__file","英语词汇.html.vue"]]);export{u as default};
